import React from 'react'
import axios from 'axios'

export class PruebaC extends React.Component{

    constructor(){
        super();
        this.state={
            datos: []
        }
    }

    componentDidMount(){
        //fetch("https://www.sanborns.com.mx/producto/getSucursales/")
        //.then(response => response.json())
        //con fetch tenemos que poner solo un res.sucursales
        axios.get("https://www.sanborns.com.mx/producto/getSucursales/")
        .then(res =>{
            console.log(res)
            this.setState({
                datos: res.data.sucursales
            })
        })
        .catch(err=>{
            console.log("error:"+err)
        })
    }

    render(){
        const {datos} = this.state;
        if(!datos || datos.length === 0) return <p>no hay datos</p>
        return(
            <div>
                {datos.map(datos1=>{
                    return(<p>{datos1.id}</p>)
                })}
            </div>
        )
    }
}

export default PruebaC

