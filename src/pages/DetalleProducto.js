import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getProductDetails } from '../redux/ducks/dataDuck';

export default function DetalleProducto(props) {

    const [qty, setQTY] = useState(1);

    const productDetails = useSelector(state=>state.productDetails);
    const {loading,error,product} = productDetails;
    const dispatch = useDispatch();

    useEffect(()=>{
        const {id} = props.match.params;
        dispatch(getProductDetails(id));
        return () =>{

        }
    },[])

    const handlerAddToCard = () =>{
        const {id} = props.match.params;
        props.history.push("/cart/"+id+"?qty="+qty)
    }

    return loading ? <div>loading...</div> :
        error ? <div>{error}</div> :
        <div>           
            <div className="breacrumb-section">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="breadcrumb-text product-more">
                                <a href="./home.html"><i className="fa fa-home"></i> Home</a>
                                <a href="./shop.html">Shop</a>
                                <span>Detail</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 
        <section className="product-shop spad page-details">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="row">
                            <div className="col-lg-6">
                                <div className="product-pic-zoom">
                                    <img className="product-big-img" src="img/product-single/product-1.jpg" alt="" />
                                    <div className="zoom-icon">
                                        <i className="fa fa-search-plus"></i>
                                    </div>
                                </div>
                                <div className="product-thumbs">
                                    <div className="product-thumbs-track ps-slider owl-carousel">
                                        <div className="pt active" data-imgbigurl="img/product-single/product-1.jpg"><img
                                                src="img/product-single/product-1.jpg" alt=""/></div>
                                        <div className="pt" data-imgbigurl="img/product-single/product-2.jpg"><img
                                                src="img/product-single/product-2.jpg" alt=""/></div>
                                        <div className="pt" data-imgbigurl="img/product-single/product-3.jpg"><img
                                                src="img/product-single/product-3.jpg" alt=""/></div>
                                        <div className="pt" data-imgbigurl="img/product-single/product-3.jpg"><img
                                                src="img/product-single/product-3.jpg" alt=""/></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="product-details">
                                    <div className="pd-title">
                                        <span>{product.category}</span>
                                        <h3>{product.title}</h3>
                                        <a href="/#" className="heart-icon"><i className="icon_heart_alt"></i></a>
                                    </div>
                                    <div className="pd-rating">
                                        {[...Array(product.rank).keys()].map(x=>
                                            <i key={x+1} className="fa fa-star"></i>
                                            )}
                                       
                                        <i className="fa fa-star-o"></i>
                                        <span>{product.rank}</span>
                                    </div>
                                    <div className="pd-desc">
                                        <p>{product.description}</p>
                                        <h4>${product.sale_price} <span>{product.price}</span></h4>
                                    </div>
                                    <div className="pd-color">
                                        <h6>Color</h6>
                                        <div className="pd-color-choose">
                                            <div className="cc-item">
                                                <input type="radio" id="cc-black"/>
                                                <label for="cc-black"></label>
                                            </div>
                                            <div className="cc-item">
                                                <input type="radio" id="cc-yellow"/>
                                                <label for="cc-yellow" className="cc-yellow"></label>
                                            </div>
                                            <div className="cc-item">
                                                <input type="radio" id="cc-violet"/>
                                                <label for="cc-violet" className="cc-violet"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="pd-size-choose">
                                        <div className="sc-item">
                                            <input type="radio" id="sm-size" />
                                            <label for="sm-size">s</label>
                                        </div>
                                        <div className="sc-item">
                                            <input type="radio" id="md-size" />
                                            <label for="md-size">m</label>
                                        </div>
                                        <div className="sc-item">
                                            <input type="radio" id="lg-size" />
                                            <label for="lg-size">l</label>
                                        </div>
                                        <div className="sc-item">
                                            <input type="radio" id="xl-size" />
                                            <label for="xl-size">xs</label>
                                        </div>
                                    </div>
                                    <div className="product-show-option">
                                        <div className="row">
                                            <div className="col-lg-7 col-md-7">
                                                <div className="select-option">
                                                    <select value={qty} onChange={(e)=> setQTY(e.target.value)} className="p-show">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                    </select>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                    <div className="quantity">
                                        <div className="pro-qty">
                                            <input type="text" value="1"/>
                                        </div>
                                        <a href="/#" onClick={handlerAddToCard} className="primary-btn pd-cart">Add To Cart</a>
                                    </div>
                                    <ul className="pd-tags">
                                        <li><span>CATEGORIES</span>: {product.description}</li>
                                        <li><span>TAGS</span>: {product.category}</li>
                                    </ul>
                                    <div className="pd-share">
                                        <div className="p-code">Sku : 00012</div>
                                        <div className="pd-social">
                                            <a href="/#"><i className="ti-facebook"></i></a>
                                            <a href="/#"><i className="ti-twitter-alt"></i></a>
                                            <a href="/#"><i className="ti-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="product-tab">
                            <div className="tab-item">
                                <ul className="nav" role="tablist">
                                    <li>
                                        <a className="active" data-toggle="tab" href="#tab-1" role="tab">DESCRIPTION</a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#tab-2" role="tab">SPECIFICATIONS</a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#tab-3" role="tab">Customer Reviews (02)</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="tab-item-content">
                                <div className="tab-content">
                                    <div className="tab-pane fade-in active" id="tab-1" role="tabpanel">
                                        <div className="product-content">
                                            <div className="row">
                                                <div className="col-lg-7">
                                                    <h5>Introduction</h5>
                                                    <p>{product.description} </p>
                                                    <h5>Features</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                                                        ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                                        aliquip ex ea commodo consequat. Duis aute irure dolor in </p>
                                                </div>
                                                <div className="col-lg-5">
                                                    <img src="img/product-single/tab-desc.jpg" alt=""/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="tab-pane fade" id="tab-2" role="tabpanel">
                                        <div className="specification-table">
                                            <table>
                                                <tr>
                                                    <td className="p-catagory">Customer Rating</td>
                                                    <td>
                                                        <div className="pd-rating">
                                                            <i className="fa fa-star"></i>
                                                            <i className="fa fa-star"></i>
                                                            <i className="fa fa-star"></i>
                                                            <i className="fa fa-star"></i>
                                                            <i className="fa fa-star-o"></i>
                                                            <span>(5)</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="p-catagory">Price</td>
                                                    <td>
                                                        <div className="p-price">${product.sale_price}</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="p-catagory">Add To Cart</td>
                                                    <td>
                                                        <div className="cart-add">+ add to cart</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="p-catagory">Availability</td>
                                                    <td>
                                                        <div className="p-stock">22 in stock</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="p-catagory">Weight</td>
                                                    <td>
                                                        <div className="p-weight">1,3kg</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="p-catagory">Size</td>
                                                    <td>
                                                        <div className="p-size">Xxl</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="p-catagory">Color</td>
                                                    <td><span className="cs-color"></span></td>
                                                </tr>
                                                <tr>
                                                    <td className="p-catagory">Sku</td>
                                                    <td>
                                                        <div className="p-code">00012</div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div className="tab-pane fade" id="tab-3" role="tabpanel">
                                        <div className="customer-review-option">
                                            <h4>2 Comments</h4>
                                            <div className="comment-option">
                                                <div className="co-item">
                                                    <div className="avatar-pic">
                                                        <img src="img/product-single/avatar-1.png" alt=""/>
                                                    </div>
                                                    <div className="avatar-text">
                                                        <div className="at-rating">
                                                            <i className="fa fa-star"></i>
                                                            <i className="fa fa-star"></i>
                                                            <i className="fa fa-star"></i>
                                                            <i className="fa fa-star"></i>
                                                            <i className="fa fa-star-o"></i>
                                                        </div>
                                                        <h5>Brandon Kelley <span>27 Aug 2019</span></h5>
                                                        <div className="at-reply">Nice !</div>
                                                    </div>
                                                </div>
                                                <div className="co-item">
                                                    <div className="avatar-pic">
                                                        <img src="img/product-single/avatar-2.png" alt=""/>
                                                    </div>
                                                    <div className="avatar-text">
                                                        <div className="at-rating">
                                                            <i className="fa fa-star"></i>
                                                            <i className="fa fa-star"></i>
                                                            <i className="fa fa-star"></i>
                                                            <i className="fa fa-star"></i>
                                                            <i className="fa fa-star-o"></i>
                                                        </div>
                                                        <h5>Roy Banks <span>27 Aug 2019</span></h5>
                                                        <div className="at-reply">Nice !</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="personal-rating">
                                                <h6>Your Ratind</h6>
                                                <div className="rating">
                                                    <i className="fa fa-star"></i>
                                                    <i className="fa fa-star"></i>
                                                    <i className="fa fa-star"></i>
                                                    <i className="fa fa-star"></i>
                                                    <i className="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                            <div className="leave-comment">
                                                <h4>Leave A Comment</h4>
                                                <form action="#" className="comment-form">
                                                    <div className="row">
                                                        <div className="col-lg-6">
                                                            <input type="text" placeholder="Name"/>
                                                        </div>
                                                        <div className="col-lg-6">
                                                            <input type="text" placeholder="Email"/>
                                                        </div>
                                                        <div className="col-lg-12">
                                                            <textarea placeholder="Messages"></textarea>
                                                            <button type="submit" className="site-btn">Send message</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    
}
