import React, { useState } from 'react'
import CheckoutSteps from '../components/CheckoutSteps';


export default function FormaDePago(props) {

    const [numeroTarjeta, setNumeroTarjeta] = useState('');
    const [nombreTitular, setNombreTitular] = useState('');
    const [FVano, setFVano] = useState('');
    const [FVmes, setFVmes] = useState('');
    const [FVcvv, setFVcvv] = useState('');

    const enviarDatos = (e) =>{
        e.preventDefault();
        console.log(numeroTarjeta,nombreTitular,FVano,FVmes,FVcvv);
        //dispatch(loginUser(email, password));
    }

    return (
        <div className="register-login-section spad">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 offset-lg-3">
                        <CheckoutSteps step1 step2 step3/>
                        <div className="login-form">
                            <br/>
                            <h2>Forma de pago</h2>
                            <form onSubmit={enviarDatos}>
                                <div className="group-input">
                                    <label for="username">Número de Tarjeta *</label>
                                    <input type="text" id="numeroTarjeta" name="numeroTarjeta" onChange={(e)=> setNumeroTarjeta(e.target.value)} />
                                </div>
                                <div className="group-input">
                                    <label for="pass">Nombre del titular de la tarjeta *</label>
                                    <input type="password" id="nombreTitular" name="nombreTitular" onChange={(e)=> setNombreTitular(e.target.value)} />
                                </div>
                                <div className="group-input">
                                    <label for="pass">Fecha de vencimiento *</label>

                                   <div className="col-md-12 row">
                                        <div className="col-md-4">
                                                <label>Año *</label>
                                                <input type="text" id="FVano" name="FVano" onChange={(e)=> setFVano(e.target.value)} />
                                        </div>
                                        <div className="col-md-4">
                                                <label>Mes *</label>
                                                <input type="text" id="FVmes" name="FVmes" onChange={(e)=> setFVmes(e.target.value)} />
                                        </div>
                                        <div className="col-md-4">
                                                <label>CVV *</label>
                                                <input type="text" id="FVcvv" name="FVcvv" onChange={(e)=> setFVcvv(e.target.value)} />
                                        </div>
                                   </div>
                                </div>
                                
                                
                                <button type="submit" className="site-btn login-btn">Siguiente</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
