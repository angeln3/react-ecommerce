import React, {useEffect} from 'react'
import {Link} from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { getProducts } from '../redux/ducks/dataDuck'
import ProductCard from '../components/ProductCard';

function Home(){

    const productList = useSelector(state => state.productList);
    const {products, loading, error} = productList;
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch(getProducts());
        return ()=>{

        }
    },[])

    return loading ? <div>Loading...</div> :
        error ? <div>{error}</div> :
        <div>
            Home
            <Link to="/Registro">Registro</Link>
            <Link to="/Login">Login</Link>
            <section className="product-shop spad">
                <div className="container">
                    <div className="row">
                        
                        <div className="col-lg-12 order-1 order-lg-2">
                        
                            <div className="product-list">
                                <div className="row">
                                    {products.map(producto=>
                                        <ProductCard producto={producto} />
                                    )}
                            </div>
                            <div className="loading-more">
                                <i className="icon_loading"></i>
                                <a href="/#">
                                    Loading More
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </section>
        </div>
}

export default Home;
