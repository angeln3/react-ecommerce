import React, {useState, useEffect} from 'react'
import axios from 'axios'

function PruebaH(){

    const [estado, setEstado] = useState({
        datos: []
    })

    useEffect(()=>{
       const fetchData = async () =>{
        const {data} = await axios.get("https://www.sanborns.com.mx/producto/getSucursales/")      
        setEstado({
            datos: data.sucursales
        })  
       }
       fetchData();
       return ()=>{}
    },[])

    if(!estado.datos || estado.datos.length === 0 ) return <p>no hay datos</p>

    return(
        <div>
            {estado.datos.map(datos1=>{
                return <p>{datos1.id}</p>
            })}

        </div>
    )
}

export default PruebaH