import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { deleteProduct, getProducts, saveProduct } from '../redux/ducks/dataDuck';

//imageUri:"hola",category:"chamarra",title:"kkkk", sale_price:123,price:123

const ProductoRegistro = () => {

    const [modalVisible, setModalVisible] = useState(false);
    const [id, setId]= useState('');
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [imageUri, setImageUri] = useState('');
    const [category, setCategory] = useState('');
    const [rank, setRank] = useState(0);
    const [price, setPrice] = useState(0); 
    const [sale_price, setSale_price] = useState(0);
    const [discount, setDiscount] = useState(0);

    const productSave = useSelector(state => state.pruductSave);
    const {loading: loadingSave, success: successSave, error: errorSave} = productSave;

    const productDelete = useSelector(state => state.productDelete);
    const {loading: loadingDelete, success: successDelete, error: errorDelete} = productDelete;

    const dispatch = useDispatch();

    const prouctList = useSelector(state=> state.productList);
    const {loading, products, error} = prouctList;

    useEffect(()=>{
        if(successSave){
            setModalVisible(false);
        }
        dispatch(getProducts());
        return () =>{}
    },[successSave, successDelete]);

    const openModal = (product) =>{
        setModalVisible(true);
        setId(product._id)
        setTitle(product.title);
        setDescription(product.description);
        setImageUri(product.imageUri);
        setCategory(product.category);
        setRank(product.rank);
        setPrice(product.price);
        setSale_price(product.sale_price);
        setDiscount(product.discount);
    };

    const deleteHandler = (product) =>{
        dispatch(deleteProduct(product._id));
    };


    const enviarProducto = (e)=>{
        e.preventDefault();
        const newProduct = {
            _id: id,
            title,
            description,
            imageUri,
            category,
            rank,
            price,
            sale_price,
            discount
        };
        dispatch(saveProduct(newProduct));
    }

    return (
        <>
        <div className="register-login-section spad">
            <button onClick={()=> openModal({})} className="site-btn register-btn">Crear producto</button>
            <div className="container">
                <div className="row">
                    {modalVisible &&
                        <div className="col-lg-6 offset-lg-3">
                        <div className="login-form">
                            <h2>Registro de productos</h2>
                            {loadingSave && <h4>Loading...</h4>}
                            {errorSave && <h4>{errorSave}</h4>}
                            {loadingDelete && <h4>Borrando...</h4>}
                            {errorDelete && <h4>Error al borrar {errorDelete}</h4>}
                            <form onSubmit={enviarProducto}>
                                <div className="group-input">
                                    <label for="username">Title *</label>
                                    <input type="text" value={title} name="title" onChange={(e)=> setTitle(e.target.value)}/>
                                </div>

                                <div className="group-input">
                                    <label for="username">Descripción *</label>
                                    <input type="text" value={description} name="Description" onChange={(e)=> setDescription(e.target.value)}/>
                                </div>

                                <div className="group-input">
                                    <label for="username">imageUri *</label>
                                    <input type="text" value={imageUri} name="imageUri" onChange={(e)=> setImageUri(e.target.value)}/>
                                </div>

                                <div className="group-input">
                                    <label for="username">Category *</label>
                                    <input type="text" value={category} name="category" onChange={(e)=> setCategory(e.target.value)}/>
                                </div>

                                <div className="group-input">
                                    <label for="username">Ranking *</label>
                                    <input type="number" value={rank} name="Rank" onChange={(e)=> setRank(Number(e.target.value))}/>
                                </div>

                                <div className="group-input">
                                    <label for="username">Precio Anterior *</label>
                                    <input type="number" value={sale_price} name="PA" onChange={(e)=> setSale_price(Number(e.target.value))}/>
                                </div>

                                <div className="group-input">
                                    <label for="username">Precio *</label>
                                    <input type="number" value={price} name="PV" onChange={(e)=> setPrice(Number(e.target.value))}/>
                                </div>      

                                <div className="group-input">
                                    <label for="username">Descuento *</label>
                                    <input type="number" value={discount} name="des" onChange={(e)=> setDiscount(Number(e.target.value))}/>
                                </div>
                                
                                <button type="submit" className="site-btn login-btn">{id ? "Modificar" : "Registrar"}</button>
                            </form>
                            <div className="switch-login">
                                <button onClick={()=>setModalVisible(false)} type="submit" className="or-login">Cancelar</button>
                            </div>
                        </div>
                    </div>
                    }
                    
                </div>
            </div>
        </div>

        <section className="shopping-cart spad">
            <div className="container">
                <div className="row">
                <div className="col-lg-12">
                <div className="cart-table">
                    <table>
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th className="p-name">Product Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        { loading ? <p>Loading...</p> 
                            : error ? <p>{error}</p> :
                            products.map(item=>
                                <tr key={item._id}>
                                    <td className="cart-title first-row">
                                        <h5>{item.imageUri}</h5>
                                    </td>
                                    <td className="cart-title first-row">
                                        <h5>{item.title}</h5>
                                    </td>
                                    <td className="cart-title first-row">
                                        <h5>{item.price}</h5>
                                    </td>
                                    <td className="cart-title first-row">
                                        <h5>{item.category}</h5>
                                    </td>
                                    <td className="cart-title first-row">
                                        <button className="primary-btn" onClick={()=>openModal(item)} >Editar</button>
                                    </td>
                                    <td className="cart-title first-row">
                                        <button className="proceed-btn" onClick={()=>deleteHandler(item)}>Borrar</button>
                                    </td>  
                                </tr>
                                )}
                            
                        </tbody>
                    </table>
                 </div>
                </div>
                </div>
            </div>
        </section>
        </>
    )
}

export default ProductoRegistro
