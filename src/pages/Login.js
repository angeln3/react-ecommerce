import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { loginUser } from '../redux/ducks/userDuck';

export default function Login(props) {

    const dispatch = useDispatch();
    const userLogin = useSelector(state=> state.userLogin);
    const {loading, userInfo, error} = userLogin;

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    useEffect(()=>{
        if(userInfo){
            props.history.push("/");
        }
        return ()=>{

        }
    },[userInfo]);

    const enviarDatos = (e) =>{
        e.preventDefault();
        dispatch(loginUser(email, password));
    }

    return (
        <div className="register-login-section spad">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 offset-lg-3">
                        <div className="login-form">
                            <h2>Login</h2>
                            {loading && <h4>Loading...</h4>}
                            {error && <h4>{error}</h4>}
                            <form onSubmit={enviarDatos}>
                                <div className="group-input">
                                    <label for="username">Username or email address *</label>
                                    <input type="text" id="username" name="email" onChange={(e)=> setEmail(e.target.value)} />
                                </div>
                                <div className="group-input">
                                    <label for="pass">Password *</label>
                                    <input type="password" id="pass" name="contraseña" onChange={(e)=> setPassword(e.target.value)} />
                                </div>
                                <div className="group-input gi-check">
                                    <div className="gi-more">
                                        <label for="save-pass">
                                            Save Password
                                            <input type="checkbox" id="save-pass"/>
                                            <span className="checkmark"></span>
                                        </label>
                                        <a href="/#" className="forget-pass">Forget your Password</a>
                                    </div>
                                </div>
                                <button type="submit" className="site-btn login-btn">Sign In</button>
                            </form>
                            <div className="switch-login">
                                <a href="./register.html" className="or-login">Or Create An Account</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
