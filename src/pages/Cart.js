import React, { useEffect } from 'react'
import queryString from 'query-string'
import { useDispatch, useSelector } from 'react-redux';
import { addToCart, removeToCart } from '../redux/ducks/cartDuck';

export default function Cart(props){
    const id = props.match.params.id;
    const query = queryString.parse(props.location.search);
    const {qty} = query;
    const dispatch = useDispatch();

    const listCart = useSelector(state=>state.cart);
    const {cartItems} = listCart;

    useEffect(()=>{
        if(id){
            dispatch(addToCart(id,qty));
        }
        return () =>{

        }
    },[]);

    const checkOut = () =>{
        props.history.push("/signin?redirect=shipping");
    }

    return( 
        <section className="shopping-cart spad">
            <div className="container">
                <div className="row">
                    <div className="col-lg-8">
                        <div className="cart-table">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th className="p-name">Product Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                        <th><i className="ti-close"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {cartItems.length === 0 ?
                                    <div>
                                        carrito bacio 
                                    </div> 
                                    :
                                    cartItems.map(item=>
                                        <tr>
                                            <td className="cart-pic first-row"><img src="img/cart-page/product-1.jpg" alt=""/></td>
                                            <td className="cart-title first-row">
                                                <h5>{item.title}</h5>
                                            </td>
                                            <td className="p-price first-row">${item.price}</td>
                                            <td className="qua-col first-row">
                                                <div className="quantity">
                                                    <div className="select-option">
                                                        <select value={item.qty} onChange={(e)=> dispatch(addToCart(item.id, e.target.value))} className="p-show">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                            <td className="total-price first-row">${item.price * item.qty}</td>
                                            <td className="close-td first-row"><i className="ti-close" onClick={()=> dispatch(removeToCart(item.id))}></i></td>
                                        </tr>
                                        )}
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="proceed-checkout">
                            <ul>
                                <li className="subtotal">Subtotal <span>{cartItems.reduce((a,c) => Number(a) + Number(c.qty), 0)}</span></li>
                                <li className="cart-total">Total <span>${cartItems.reduce((a,c)=> a + c.price * c.qty, 0)}</span></li>
                            </ul>
                            <a href="/#" className="proceed-btn" onClick={checkOut}>PROCEED TO CHECK OUT</a>
                        </div>
                        <div className="cart-buttons">
                        </div>
                        <div className="cart-buttons center-block">
                            <a href="/#" className="primary-btn continue-shop">Continue shopping</a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
    )
}