import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { registerUser } from '../redux/ducks/userDuck';

export default function Registro(props) {

    const dispatch = useDispatch();
    const userRegister = useSelector(state=> state.userRegister);
    const {loading, userInfo, error} = userRegister;

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [repassword, setRePassword] = useState('');

    useEffect(()=>{
        if(userInfo){
            props.history.push("/");
        }
        return ()=>{

        }
    },[userInfo]);

    const enviarDatos = (e) =>{
        e.preventDefault();
        dispatch(registerUser(name,email, password));
    }

    return (
        <div className="register-login-section spad">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 offset-lg-3">
                        <div className="register-form">
                            <h2>Registro</h2>
                            {loading && <h4>Loading...</h4>}
                            {error && <h4>{error}</h4>}
                            <form onSubmit={enviarDatos}>
                                <div className="group-input">
                                    <label for="username">User name *</label>
                                    <input type="text" name="usuario" onChange={(e)=> setName(e.target.value)}/>
                                </div>
                                <div className="group-input">
                                    <label for="username">email address *</label>
                                    <input type="text" id="email" name="email" onChange={(e)=> setEmail(e.target.value)}/>
                                </div>
                                <div className="group-input">
                                    <label for="pass">Password *</label>
                                    <input type="text" id="pass" name="contraseña" onChange={(e)=> setPassword(e.target.value)}/>
                                </div>
                                <div className="group-input">
                                    <label for="con-pass">Confirm Password *</label>
                                    <input type="text" id="con-pass" onChange={(e)=> setRePassword(e.target.value)}/>
                                </div>
                                <button type="submit" className="site-btn register-btn">REGISTER</button>
                            </form>
                            <div className="switch-login">
                                <Link className="or-login" to="/Login">Or Login</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
