import React, { useState } from 'react'
import CheckoutSteps from '../components/CheckoutSteps';


export default function Shipping(props) {

    const [calle, setCalle] = useState('');
    const [numeroExt, setNumeroExt] = useState('');
    const [numeroInt, setNumeroInt] = useState('');
    const [CP, setCP] = useState('');
    const [colonia, setColonia] = useState('');
    const [delegacion, setDelegacion] = useState('');
    const [estado, setEstado] = useState('');
    const [ciudad, setCiudad] = useState('');


    const enviarDatos = (e) =>{
        e.preventDefault();
        console.log(calle,numeroExt,numeroInt,CP,colonia,delegacion,estado,ciudad);
        //dispatch(loginUser(email, password));
    }

    return (
        <div className="register-login-section spad">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 offset-lg-3">
                        <CheckoutSteps step1 step2 />
                        <div className="login-form">
                            <br/>
                            <h2>Dirección de envío</h2>
                            <form onSubmit={enviarDatos}>
                                <div className="group-input">
                                    <label for="username">Calle *</label>
                                    <input type="text" id="calle" name="calle" onChange={(e)=> setCalle(e.target.value)} />
                                </div>
                                <div className="group-input">
                                    <label for="pass">Número Exterior *</label>
                                    <input type="password" id="numeroExt" name="numeroExt" onChange={(e)=> setNumeroExt(e.target.value)} />
                                </div>
                                <div className="group-input">
                                    <label for="pass">Número Interior *</label>
                                    <input type="password" id="numeroInt" name="numeroInt" onChange={(e)=> setNumeroInt(e.target.value)} />
                                </div>
                                <div className="group-input">
                                    <label for="pass">Codigo Postal *</label>
                                    <input type="password" id="cp" name="cp" onChange={(e)=> setCP(e.target.value)} />
                                </div>
                                <div className="group-input">
                                    <label for="pass">Colonia *</label>
                                    <input type="password" id="colonia" name="colonia" onChange={(e)=> setColonia(e.target.value)} />
                                </div>
                                <div className="group-input">
                                    <label for="pass">Municipio ó Delegación *</label>
                                    <input type="password" id="Delegacion" name="Delegacion" onChange={(e)=> setDelegacion(e.target.value)} />
                                </div>
                                <div className="group-input">
                                    <label for="pass">Estado *</label>
                                    <input type="password" id="Estado" name="Estado" onChange={(e)=> setEstado(e.target.value)} />
                                </div>
                                <div className="group-input">
                                    <label for="pass">Ciudad *</label>
                                    <input type="password" id="ciudad" name="ciudad" onChange={(e)=> setCiudad(e.target.value)} />
                                </div>
                                
                                <button type="submit" className="site-btn login-btn">Siguiente</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
