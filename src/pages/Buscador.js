import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import ProductCard from '../components/ProductCard'
import { getBusquedaProductos } from '../redux/ducks/dataDuck'
import queryString from 'query-string'

const Buscador = (props) => {

    const query = queryString.parse(props.location.search);
    const {q} = query;

    const productList = useSelector(state=> state.productSearch);
    const {products, loading, error} = productList;

    const dispatch = useDispatch();

    useEffect(()=>{
        
        dispatch(getBusquedaProductos(q));

        return () =>{}
    },[]);

    return loading ? <div>Loading...</div> :
        error ? <div>{error}</div> :
        <div>
            <section className="product-shop spad">
                <div className="container">
                    <div className="row">
                        
                        <div className="col-lg-12 order-1 order-lg-2">
                        
                            <div className="product-list">
                                <div className="row">
                                    {products.map(producto=>
                                        <ProductCard producto={producto} />
                                    )}
                            </div>
                            <div className="loading-more">
                                <i className="icon_loading"></i>
                                <a href="/#">
                                    Loading More
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </section>
        </div>
}

export default Buscador
