import { mount, shallow } from 'enzyme';
import React from 'react';
import App from './App';
import Header from './components/Header';
import ProductCard from "./components/ProductCard";

const cardList ={
  _id: 12334,
  category: 'chamarra',
  price: 199,
  sale_price: 100,
  imageUri: 'ruta'
}

describe("rendering components", ()=>{
  it("renders App component without crashing", ()=>{
    shallow(<App/>);
  });
  it("renders App component Header without crashing", ()=>{
    const wrapper = shallow(<App />);
    const header = (<Header/>);
    expect(wrapper.contains(header)).toEqual(true);
  });
})

describe("passing props", ()=>{
  const productCardWrapper = mount(<ProductCard producto={cardList} />);
  it("accept card props", ()=>{
    expect(productCardWrapper.props().producto).toEqual(cardList);
  });
});