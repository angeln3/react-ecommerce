import Axios from "axios";
import Cookie from "js-cookie";

//constantes
const CART_ADD_ITEM = "CART_ADD_ITEM";
const CARD_REMOVE_ITEM = "CARD_REMOVE_ITEM";

//reducers
export function cartReducer(state = { cartItems:[] } , action){
    switch(action.type){
        case CART_ADD_ITEM:
            const item = action.payload;
            const product = state.cartItems.find(x => x.id === item.id);
            console.log(product);
            if(product){
                console.log("ENTRÓ");
                return {
                    cartItems: state.cartItems.map(x => x.id === product.id ? item : x)
                }
            }
            return { cartItems: [...state.cartItems, item] };

        case CARD_REMOVE_ITEM:
            return { cartItems: state.cartItems.filter(x => x.id !== action.payload ) }
        default:
            return state
    }

}

//actions

export const addToCart = (id,qty) => async (dispatch, getState) =>{
    try {
        const {data} = await Axios.get("http://localhost:5000/articles/"+id);
        dispatch({
            type: CART_ADD_ITEM, payload:{
                id: data._id,
                title: data.title,
                imageUri: data.imageUri,
                price: data.price,
                qty
            }
        });
        const {cart: {cartItems} } = getState();
        Cookie.set("cartItems", JSON.stringify(cartItems));
    } 
    catch (error) {
        console.log(error);
    }
}

export const removeToCart = (id) => (dispatch, getState) =>{
    dispatch({type: CARD_REMOVE_ITEM, payload: id});

    const {cart: {cartItems} } = getState();
    Cookie.set("cartItems", JSON.stringify(cartItems));
}