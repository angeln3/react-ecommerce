import axios from 'axios';
import Cookie from "js-cookie";

//cosntantes
const USER_LOGIN_REQUEST = "USER_LOGIN_REQUEST";
const USER_LOGIN_SUCCESS = "USER_LOGIN_SUCCESS";
const USER_LOGIN_ERROR = "USER_LOGIN_ERROR";

const USER_REGISTER_REQUEST = "USER_REGISTER_REQUEST";
const USER_REGISTER_SUCCESS = "USER_REGISTER_SUCCESS";
const USER_REGISTER_ERROR = "USER_REGISTER_ERROR";

//reducer

export function userLoginReducer(state= {}, action){
    switch(action.type){
        case USER_LOGIN_REQUEST:
            return { loading: true };
        case USER_LOGIN_SUCCESS:
            return { loading: false, userInfo: action.payload };
        case USER_LOGIN_ERROR:
            return { loading: false, error: action.payload };
        default:
            return state;    
    }
}

export function userRegisterReducer(state= {}, action){
    switch(action.type){
        case USER_REGISTER_REQUEST:
            return { loading: true };
        case USER_REGISTER_SUCCESS:
            return { loading: false, userInfo: action.payload };
        case USER_REGISTER_ERROR:
            return { loading: false, error: action.payload };
        default:
            return state;    
    }
}


//action

export const loginUser = (email, password) => async(dispatch) =>{
    dispatch( {type: USER_LOGIN_REQUEST, payload: {email, password} });
    try {
        const {data} = await axios.post("http://localhost:5000/login", {email, password});
        dispatch( {type: USER_LOGIN_SUCCESS, payload: data} );
        Cookie.set('userInfo', JSON.stringify(data));
    } catch (error) {
        dispatch( {type:USER_LOGIN_ERROR, payload: error.message} )
    }
}

export const registerUser = (name, email, password) => async(dispatch) =>{
    dispatch( {type: USER_REGISTER_REQUEST });
    try {
        const {data} = await axios.post("http://localhost:5000/register", {name, email, password});
        dispatch( {type: USER_REGISTER_SUCCESS, payload: data} );
        Cookie.set('userInfo', JSON.stringify(data));
    } catch (error) {
        dispatch( {type:USER_REGISTER_ERROR, payload: error.message} )
    }
}