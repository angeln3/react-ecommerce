import axios from 'axios'


//constantes
const PRODUCT_LIST_REQUEST = "PRODUCT_LIST_REQUEST";
const PRODUCT_LIST_SUCCESS = "PRODUCT_LIST_SUCCESS";
const PRODUCT_LIST_ERROR = "PRODUCT_LIST_ERROR";

const PRODUCT_DETAILS_REQUEST = "PRODUCT_DETAILS_REQUEST";
const PRODUCT_DETAILS_SUCCESS = "PRODUCT_DETAILS_SUCCESS";
const PRODUCT_DETAILS_ERROR = "PRODUCT_DETAILS_ERROR";

const PRODUCT_SEARCH_REQUEST = "PRODUCT_SEARCH_REQUEST";
const PRODUCT_SEARCH_SUCCESS = "PRODUCT_SEARCH_SUCCESS";
const PRODUCT_SEARCH_ERROR = "PRODUCT_SEARCH_ERROR";

const PRODUCT_SAVE_REQUEST = "PRODUCT_SAVE_REQUEST";
const PRODUCT_SAVE_SUCCESS = "PRODUCT_SAVE_SUCCESS";
const PRODUCT_SAVE_ERROR = "PRODUCT_SAVE_ERROR";

const PRODUCT_DELETE_REQUEST = "PRODUCT_DELETE_REQUEST";
const PRODUCT_DELETE_SUCCESS = "PRODUCT_DELETE_SUCCESS";
const PRODUCT_DELETE_ERROR = "PRODUCT_DELETE_ERROR";

//reducer
export function producListReducer(state = { products: [] }, action){
    switch(action.type){
        case PRODUCT_LIST_REQUEST:
            return {loading: true, products: []};
        case PRODUCT_LIST_SUCCESS:
            return {
                loading: false,
                products: action.payload
                };
        case PRODUCT_LIST_ERROR:
            return {
                loading: false,
                error: action.payload
            }
        default:
            return state
    }

}

export function productDetailsReducer(state = { product:{} }, action){
    switch(action.type){
        case PRODUCT_DETAILS_REQUEST:
            return {loading: true}
        case PRODUCT_DETAILS_SUCCESS:
            return {
                loading: false,
                product: action.payload
            }
        case PRODUCT_DETAILS_ERROR:
            return {
                loading: false,
                error: action.payload
            }
        default:
            return state
    }
}

export function productSearchReducer(state = { products: [] }, action){
    switch(action.type){
        case PRODUCT_SEARCH_REQUEST:
            return {loading: true}
        case PRODUCT_SEARCH_SUCCESS:
            return {
                loading: false,
                products: action.payload
            }
        case PRODUCT_SEARCH_ERROR:
            return {
                loading: false,
                error: action.payload
            }
        default:
            return state
    }
}

export function productSaveReducer(state = { product:{} }, action){
    switch(action.type){
        case PRODUCT_SAVE_REQUEST:
            return {loading: true}
        case PRODUCT_SAVE_SUCCESS:
            return {
                loading: false,
                success: true,
                product: action.payload
            }
        case PRODUCT_SAVE_ERROR:
            return {
                loading: false,
                error: action.payload
            }
        default:
            return state
    }
}

export function productDeleteReducer(state = { product: {} }, action) {
    switch (action.type) {
        case PRODUCT_DELETE_REQUEST:
            return {
                loading: true
            }
        case PRODUCT_DELETE_SUCCESS:
            return {
                loading: false,
                success: true,
                product: action.payload
            }
        case PRODUCT_DELETE_ERROR:
            return{
                loading: false,
                error: action.payload
            }    
              
        default:
            return state;
    }
}

//actions
//getState para obtener el satate global

export const getProducts = () => async (dispatch,getState) => {
    try{
        dispatch({type:PRODUCT_LIST_REQUEST});
        const {data} = await axios.get("http://localhost:5000/articles");
        dispatch({type:PRODUCT_LIST_SUCCESS, payload: data.article});
    }
    catch(error){
        dispatch({type:PRODUCT_LIST_ERROR, payload: error.message});
    }
    
}

export const getProductDetails = (id) => async (dispatch) =>{
    try{
        dispatch({type: PRODUCT_DETAILS_REQUEST });
        const {data} = await axios.get("http://localhost:5000/articles/"+id);
        dispatch({type: PRODUCT_DETAILS_SUCCESS, payload: data });
    }
    catch(error){
        dispatch({type: PRODUCT_DETAILS_ERROR, payload: error.message })
    }
    
}

export const getBusquedaProductos = (query) => async(dispatch)=>{

    try {
        dispatch({type: PRODUCT_SEARCH_REQUEST});
        const {data} = await axios.get(`http://localhost:5000/Busqueda?q=${query}`);
        dispatch({type: PRODUCT_SEARCH_SUCCESS, payload: data.article });   
    } 
    catch (error) {
        dispatch({type: PRODUCT_SEARCH_ERROR, payload: error.message})
    }
   
}  

export const saveProduct = (product) => async(dispatch, getState)=>{

    try {
        dispatch({type: PRODUCT_SAVE_REQUEST});
        const {userLogin: {userInfo}} = getState();
        if(!product._id){
            var datares = await fetch("http://localhost:5000/new-article/", {
                method: "POST",
                body: JSON.stringify(product),
                headers: {
                  "Content-Type": "application/json",
                  //"x-api-key": "8f0679e6-5a6f-4b78-b46f-8486402e71c4",
                }
              });
              var data = await datares.json();
            dispatch({type: PRODUCT_SAVE_SUCCESS, payload: data});
        }else{
            const {data} = await axios.put("http://localhost:5000/new-article/"+product._id, product
            //,
                //{
                //headers:{
                    //'Authorization': 'Bearer ' + userInfo.token
                //},
            //}
            );
            dispatch({type: PRODUCT_SAVE_SUCCESS, payload: data});
        }
        
    } catch (error) {
        dispatch({type: PRODUCT_SAVE_ERROR, payload: error.message})
    }

} 

export const deleteProduct = (id) => async(dispatch) =>{
    try {
        dispatch({type:PRODUCT_DELETE_REQUEST});
        const {data} = await axios.delete("http://localhost:5000/articles/borrar/"+id);
        dispatch({type: PRODUCT_DELETE_SUCCESS, payload: data, success: true});
                
    } catch (error) {
        dispatch({type:PRODUCT_DELETE_ERROR, payload: error.message})
    }

}