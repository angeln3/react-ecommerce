import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
//Middleware
import thunk from 'redux-thunk';
//Cookie
import Cookie from 'js-cookie';
//Reducers
import {producListReducer, productDeleteReducer, productDetailsReducer, productSaveReducer, productSearchReducer} from './ducks/dataDuck';
import { cartReducer } from './ducks/cartDuck';
import { userLoginReducer, userRegisterReducer } from './ducks/userDuck';

const cartItems = Cookie.getJSON("cartItems") || [];
const userInfo = Cookie.getJSON("userInfo") || null;

const initialState = { cart: {cartItems}, userLogin: {userInfo} };

const middleware = [thunk];

const reducers = combineReducers({
    productList: producListReducer,
    productDetails: productDetailsReducer,
    cart: cartReducer,
    userLogin: userLoginReducer,
    userRegister: userRegisterReducer,
    productSearch: productSearchReducer,
    pruductSave: productSaveReducer,
    productDelete: productDeleteReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    reducers,
    initialState,
    composeEnhancers(
        applyMiddleware(...middleware),
    )
);

export default store;