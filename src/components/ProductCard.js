import React from 'react'
import {Link} from 'react-router-dom'

export default function ProductCard({producto}) {
    return (
        <div className="col-lg-3 col-sm-6">
            <div className="product-item">
                <div className="pi-pic">
                    <img src={producto.imageUri} alt=""/>
                    <div className="icon">
                        <i className="icon_heart_alt"></i>
                    </div>
                    <ul>
                        <li className="w-icon active"><a href="/#"><i className="icon_bag_alt"></i></a></li>
                        <li className="quick-view"><a href="/#">+ Quick View</a></li>
                        <li className="w-icon"><a href="/#"><i className="fa fa-random"></i></a></li>
                    </ul>
                </div>
                <div className="pi-text">
                    <div className="catagory-name">{producto.category}</div>
                    <Link to={"Producto/"+producto._id}>
                        <h5>{producto.title}</h5>
                    </Link>
                    <div className="product-price">
                        ${producto.sale_price}
                        <span>${producto.price}</span>
                    </div>
                </div>
            </div>
        </div>
    )
}
