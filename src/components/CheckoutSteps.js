import React from 'react'

const CheckoutSteps = (props) => {

    return (
        <span className="checkoutSteps">
            <span className={props.step1 ? 'active' : ''}>Login</span>
            <span className={props.step2 ? 'active' : ''}>Dirección de envío</span>
            <span className={props.step3 ? 'active' : ''}>Forma de pago</span>
            <span className={props.step4 ? 'active' : ''}>Confirmación</span>
        </span>
    )
}

export default CheckoutSteps
