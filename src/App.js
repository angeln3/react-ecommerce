import React from 'react';
//route
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
//import "./scss/main.scss"
import './css/styles.css'
//components
import Header from './components/Header'
import Footer from './components/Footer'

//componentes página
import Home from './pages/Home'
import Login from './pages/Login'
import Registro from './pages/Registro'
import Product from './pages/DetalleProducto'
import Cart from './pages/Cart'
import FormaDePago from './pages/FormaDePago'
//import PruebaC from './pages/PruebaC'
//import PruebaH from './pages/PruebaH'
import Buscador from './pages/Buscador'
import ProductoRegistro from './pages/ProductoRegistro'
import Shipping from './pages/Shipping'
//redux
import {Provider} from 'react-redux'
import store from './redux/store'



function App() {
  return (
    <body>
      <Provider store={store}>
        <Router>
          <Header/>
            <Switch>
              <Route exact={true} path="/" component={Home} />
              <Route exact path="/Producto/:id" component={Product} />
              <Route exact path="/cart/:id?" component={Cart} />
              <Route exact path="/Registro" component={Registro} />
              <Route exact path="/Login" component={Login} />
              <Route exact path="/Buscar" component={Buscador} />
              <Route exact path="/RegistroProducto" component={ProductoRegistro} />
              <Route exact path="/Shipping" component={Shipping} />
              <Route exact path="/FormaDePago" component={FormaDePago} />
            </Switch>
          <Footer/>
        </Router>
      </Provider>
    </body>
  );
}

export default App;
